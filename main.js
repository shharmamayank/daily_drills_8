/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.
*/

const fs = require("fs");
const path = require("path");
function RetrieveData(idArray,data){
    fs.readFile(path.join(__dirname,data),(err,data)=>{
        if(err){
        err =new Error('error while reading')
        }else{
            let dataIds= JSON.parse(data).employees.filter((data)=>{
                return idArray.includes(data.id)
            })
            console.log(dataIds)
        }
    })
    fs.writeFile(path.join(__dirname,"output.json"),JSON.stringify(dataIds),(err,data)=>{
        if(err){
            err =new Error('error writing file')  
        }else{
            console.log("file written");
        }
    })
}
RetrieveData([1],"data.json")
